# Item information
`NP1`, `NP2` and `NP3` directories include the data of 3 examples, i.e., **Patient 1**, **Patient 2** and **Patient 3** shown in **Fig. 3**, respectively.
- `NP*_Ablation.par`: parameter files required for simulations with openCARP
- `NP*_Ablation-FibElems.regele`, `NP*_Ablation-NonFibElems.regele`, `NP*_Ablation-IsAblated.adjust`, `NP*_Ablation-RestingVm.adjust`: additional files required for simulations with openCARP
- `NP*-DT.vtk`: files of **full mesh of DT** containing the following items which can be selected using the selection box under the "Coloring" section in the "Display" tab
  - `elem Tag`: fibrotic tissue in RA (32) and LA (128), normal tissue in RA (64) and LA (255)
  - `fiber`: mapped fiber
- `NP*_LRs.pvsm`: state files of **identified rotor locations** containing multiple transmembrane potentials which can be selected using the selection box under the "Coloring" section in the "Display" tab
  - `vm_1`: rotor location (A)
  - `vm_2`: rotor location (B)
  - `vm_3`: rotor location (C)
  - `vm_4`: rotor location (D)
- `NP*_class.pvsm`: state files of **classification of rotor locations** into LIRs and LCRs
- `NP*_fibrosis`: state files of **fibrosis distribution**