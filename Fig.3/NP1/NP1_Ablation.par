num_phys_regions         =  1
phys_region[0].ptype     =  0
phys_region[0].num_IDs   =  4
phys_region[0].ID        =  32 64 128 255

simID                    = NP1_Ablation
start_statef             = ""

dt                       = 50
timedt                   = 50
spacedt                  = 20
tend                     = 7500
chkpt_start              = 2500
chkpt_intv               = 1000
chkpt_stop               = 7500
experiment               = 0
mat_entries_per_row      = 60
parab_solve              = 1
mass_lumping             = 0
bidm_eqv_mono            = 0
t_sentinel               = 220
sentinel_ID              = 0

num_LATs                 = 1
lats[0].all              = 1
lats[0].method           = 1
lats[0].measurand        = 0
lats[0].threshold        = -40

# ------------------------------------------------------------------------------
# Ablation-related parameters
# ------------------------------------------------------------------------------

numtagreg                = 2

tagreg[0].tag            = 1024
tagreg[0].type           = 3
tagreg[1].tag            = 512
tagreg[1].type           = 3

tagreg[0].elemfile       = NP1_Ablation-NonFibElems
tagreg[1].elemfile       = NP1_Ablation-FibElems

num_adjustments          = 3

adjustment[0].variable   = JB_Courtemanche_cAF_v2.ABLATED
adjustment[1].variable   = JB_Courtemanche_cAF_v2.ABLATED_Vrest
adjustment[2].variable   = Vm

adjustment[0].file       = NP1_Ablation-IsAblated.adjust
adjustment[1].file       = NP1_Ablation-RestingVm.adjust
adjustment[2].file       = NP1_Ablation-RestingVm.adjust

# ------------------------------------------------------------------------------
# Ionic model setup
# ------------------------------------------------------------------------------

num_external_imp         = 1

num_imp_regions          = 2

imp_region[0].name       = non-fibrotic myocardium
imp_region[0].im         = JB_Courtemanche_cAF_v2
imp_region[0].num_IDs    = 3
imp_region[0].ID[0]      = 64
imp_region[0].ID[1]      = 255
imp_region[0].ID[2]      = 1024
imp_region[0].im_param   = "flags=CAF"
imp_region[0].im_sv_init = ""

imp_region[1].name       = fibrotic myocardium
imp_region[1].im         = JB_Courtemanche_cAF_v2
imp_region[1].num_IDs    = 3
imp_region[1].ID[0]      = 32
imp_region[1].ID[1]      = 128
imp_region[1].ID[2]      = 512
imp_region[1].im_param   = "flags=FIBCAF"
imp_region[1].im_sv_init = ""

# ------------------------------------------------------------------------------
# Tissue conductivity setup
# ------------------------------------------------------------------------------

num_gregions             = 3

gregion[2].g_mult        = 1e-6
gregion[2].num_IDs       = 2
gregion[2].ID[0]         = 1024
gregion[2].ID[1]         = 512

gregion[0].name          = non-fibrotic myocardium
gregion[0].g_il          = 0.1920
gregion[0].g_it          = 0.0384
gregion[0].g_in          = 0.0384
gregion[0].g_mult        = 0.65870094299316406250
gregion[0].num_IDs       = 2
gregion[0].ID[0]         = 64
gregion[0].ID[1]         = 255

gregion[1].name          = fibrotic myocardium
gregion[1].g_il          = 0.1920
gregion[1].g_it          = 0.0240
gregion[1].g_in          = 0.0240
gregion[1].g_mult        = 0.284667968750
gregion[1].num_IDs       = 2
gregion[1].ID[0]         = 32
gregion[1].ID[1]         = 128

# ------------------------------------------------------------------------------
# INPUT: (1) Location for all current stimuli
#        (2) timing of current stimuli -- dynamic pacing is used to induce AF; 
#            the gradually decreasing interval between paces should lead to 
#            conduction block and arrhythmogenesis from some sites.
# ------------------------------------------------------------------------------

num_stim                 = 1

stim[0].ptcl.stimlist    = "0,225,450,670,885,1095,1300,1500,1695,1885,2070,2250,2425,2600,2775,2950"
stim[0].pulse.strength   = 3e2
stim[0].ptcl.duration    = 5
stim[0].crct.type        = 0