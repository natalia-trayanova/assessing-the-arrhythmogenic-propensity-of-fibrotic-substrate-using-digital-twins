# Assessing the Arrhythmogenic Propensity of Fibrotic Substrate using Digital Twins

## Description
Provide the computational meshes and the parameter files required for simulations with openCARP and the source data used to produce analysis in Fig.3 based on the simulation results as well as the data and codes for the analysis shown in Figs. 2, 5, and 6 for Sakata et al. *Nature Cardiovascular Research (2024)*. The open-source software ParaView (https://www.paraview.org) and the freely available software EZR, graphical user interface for R (The R Foundation for Statistical Computing, Vienna, Austria) can be used to visualize the data and analyze the results, respectively.

### Getting started
All files have been tested on macOS Sonoma v14.4.1
- Download and install **ParaView v5.11** (https://www.paraview.org/download/)
  1. Run ParaView, then select "Load State..." from the "File" menu
  2. Navigate to the directory containing `.pvsm` files and select the file of interest
  3. Press "OK" to initiate the loading process, then a new window entitled "Load State Options" will appear
  4. Select the "Search files under specified directory" option from the first drop-down menu and then point the "Data directory" field to the directory containing `.pvsm` files
  5. Visualize 3D data sets

- Download and install **EZR v1.55** (https://www.jichi.ac.jp/saitama-sct/SaitamaHP.files/statmedEN.html)
  1. Run EZR, then select "Load data set" from the "File" menu
  2. Navigate to the directory containing `.rda` file, select the file of interest, and press "Open" to load the data set
  3. Select "Load script file" from the "File" menu
  4. Navigate to the directory containing `.R` file, select the file of interest, and press "Open" to load the R script
  5. In the upper "R script" window, point "Select all" from the "Edit" menu, then press "Submit" buttun between "R script" and "Output" windows
  6. Display statistic analysis in the "Output" window and generate another window showing the graph


### Data files
Each directory has the data file shown in Figs and its description file.
Due to the repository storage limit, other mesh files and simulations files are available upon reasonable request to the corresponding author, Prof. Natalia Trayanova (ntrayanova@jhu.edu). Raw scan image data and clinical electroanatomic mapping data cannot be readily shared due to patient privacy concerns. Specific requests for access to these data may be made in consultation with the Johns Hopkins Institutional Review Board.